import Question from "./question.js";

class Fillinblank extends Question {
  constructor(...arg) {
    super(...arg);
  }

  render(index) {
    return `
        <h5>Câu hỏi ${index}:</h5>
        <input type="text" id="question-${this._id}"/>
        `;
  }
  checkExact(){
      let answer = document.getElementById(`question-${this._id}`).value;
      
        return answer.toString() === this.answers[0].content;
  }
}

export default Fillinblank;
