import Question from "./question.js";

class Multiplechoise extends Question {
  constructor(...arg) {
    super(...arg);
  }
  renderAnswers() {
    var answerContent = "";
    for (let ans of this.answers) {
      answerContent += `
            <div>
                <input type="radio" class="question-${this._id}" value="${
        ans._id
      }" name="${this._id}"/>
                <span>${ans.content}</span>
            </div>
            `;
    }
    return answerContent;
  }
  render(index) {
    return `
        <h5>Câu hỏi${index}</h5>
        ${this.renderAnswers()}
        `;
  }
  checkExact() {
    let exact = false;
    [...document.getElementsByClassName(`question-${this._id}`)].forEach(
      ans => {
        if (ans.checked) {
          const ansID = ans.value;
          const checkedAnswer = this.answers.find(ans => {
            return ans._id === ansID.toString();
          }) || {};
          exact = checkedAnswer.exact || false;
        }
      }
    );
    return exact;
  }
}

export default Multiplechoise;
