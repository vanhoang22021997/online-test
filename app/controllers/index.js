import Fillinblank from "../models/fillinblank.js";
import Multiplechoise from "../models/multiplechoise.js";

var questionList = [];

const getData = () => {
  axios({
    url: "../../assets/dethitracnghiem.json",
    method: "GET"
  })
    .then(res => {
      questionList = res.data.map(currentQuestion => {
        const { questionType, _id, content, answers } = currentQuestion;
        if (questionType === 1) {
          var newQuestion = new Multiplechoise(
            questionType,
            _id,
            content,
            answers
          );
        } else {
          newQuestion = new Fillinblank(questionType, _id, content, answers);
        }
        return newQuestion;
      });
      
      renderQuestion();
    })
    .catch(err => {
      console.log(err);
    });
};

const renderQuestion = () => {
  let content = "";
  for (let index in questionList) {
    content += questionList[index].render((index = index * 1 + 1));
  }
  document.getElementById("content").innerHTML = content;
};

document.getElementById("btnSubmit").addEventListener("click", () => {
  let result = questionList.reduce((sum, currentQuestion) => {
    return (sum += currentQuestion.checkExact());
  }, 0);
  console.log(result);
});

getData();
